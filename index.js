const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const {
  findAndCountAll,
  findById,
  create,
  update,
  remove
} = require('./personas');
// async-await 

function respuesta(codigo, finalizado, data) {
  return {
    finalizado,
    mensaje: codigo === 200 ? 'OK' : 'ERROR',
    data
  }
}

app.get('/personas', async (req, res) => {
  const personas = await findAndCountAll();
  res.status(200).json(respuesta(200, true, personas));
});

app.get('/personas/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const personaBuscada = await findById(parseInt(id));
    res.status(200).json(respuesta(200, true, personaBuscada));
  } catch (error) {
    res.status(400).json(respuesta(400, false, error))
  }
});

// params, query, BODY
app.post('/personas', async (req, res) => {
  try {
    const data = req.body;
    const personaCreada = await create(data);
    res.status(200).json(respuesta(200, true, personaCreada));
  } catch (error) {
    res.status(400).json(respuesta(400, false, error));
  }
});

app.put('/personas/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const data = req.body;
    const personaActualizada = await update(parseInt(id), data);
    res.status(200).json(respuesta(200, true, personaActualizada));
  } catch (error) {
    res.status(400).json(respuesta(400, false, error))
  }
});

app.delete('/personas/:id', async (req, res) => {
  try {
    const { id } = req.params;
    await remove(parseInt(id));
    res.status(200).json(respuesta(200, true, true))
  } catch (error) {
    res.status(400).json(respuesta(400, false, error))
  }
});



// status: Muestra el estado de los cambios.
// add: Adiciona los cambios a subir al repositorio.
// commit: que pone el mensaje para subir al repositorio.
// push: Subira los cambios al repositorio.
// pull: Bajara (Actualizara) los cambios desde el repositorio.

app.listen(port, () => {
  console.log('==============================_SERVIDOR_==============================');
  console.log('Funcionando correctamente');
  console.log('==============================_SERVIDOR_==============================');
})